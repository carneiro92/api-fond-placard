import { DaoRecipes } from "../DAO/DaoRecipes";
import { Recipes } from "../Entity/Recipes";

export class RecipesBusiness {
    private repo:DaoRecipes
    constructor() {
        this.repo = new DaoRecipes();
    }
    async getAll() {

        let recipes = await this.repo.getAll();
        return recipes;
    }
    async getByID(id:number){
        let recipe = await this.repo.getByID(id);
        return recipe;
    }
    async getByName(name:string){      
        let recipe = await this.repo.getByName(name);
        return recipe;
    }
    async add(recipe:Recipes) {

        let recipes = await this.repo.add(recipe);
        return recipe;
    }
    async update(recipe:Recipes){
        let upRecipe = await this.repo.update(recipe);
        console.log(upRecipe);
        
        return upRecipe
    }
    async upvote(recipe:Recipes){
        recipe.setScore(recipe.getScore()+1)
        let upRecipe = await this.repo.update(recipe);        
        return upRecipe
    }
    async downvote(recipe:Recipes){
        recipe.setScore(recipe.getScore()-1)
        let upRecipe = await this.repo.update(recipe);        
        return upRecipe
    }
    async delete(recipe:Recipes){
        await this.repo.delete(recipe)
        return recipe
    }
}