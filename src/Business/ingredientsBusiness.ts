import { DaoIngredients } from "../DAO/DAOIngredients";
import { Ingredients } from "../Entity/Ingredients";

export class IngredientsBusiness {
    private repo:DaoIngredients
    constructor() {
        this.repo = new DaoIngredients();
    }
    async getAll() {

        let ingredients = await this.repo.getAll();
        return ingredients;
    }
    async getByID(id:number) :Promise<Ingredients>{
        let ingredient = await this.repo.getByID(id);
        return ingredient;
    }
    async getByName(name:string){      
        let ingredient = await this.repo.getByName(name);
        return ingredient;
    }
    async add(ingredient:Ingredients) {

        let ingredients = await this.repo.add(ingredient);
        return ingredient;
    }
    async update(req: any){
        let ingredient =  await this.getByID(req.params.id)
        ingredient.update(req.body)
        let upIngredients = await this.repo.update(ingredient);       
        return upIngredients
    }
    async delete(ingredient:Ingredients){
        await this.repo.delete(ingredient)
        return ingredient
    }
}
