import { connection } from "../ConnectDB/ConnectDB"
import { promisify } from "util";
import { Ingredients } from "../Entity/Ingredients";




export class DaoIngredients {
    private query;
    constructor() {
        this.query = promisify(connection.query).bind(connection);
    }
    async getAll(): Promise<Ingredients[]> {
        let result = await this.query("SELECT * FROM ingredients");

        return result.map(row => new Ingredients(row['name'], row['id']));

    }
    async getByID(id:number):Promise<Ingredients> {
        let results = await this.query("SELECT * FROM ingredients WHERE id=?",[id]);
        if(results.length > 0) {
            return new Ingredients(results[0]['name'], results[0]['id']);
        }
        return null;
    }
    async getByName(name:string):Promise<Ingredients> {
        let results = await this.query("SELECT * FROM ingredients WHERE name LIKE ?",[name]);
        if(results.length > 0) {
            return new Ingredients(results[0]['name'], results[0]['id']);
        }
        return null;
    }
    async add(ingredients: Ingredients): Promise<number> {
        let result = await this.query('INSERT INTO ingredients (name) VALUES (?)', [
            ingredients.getName(),
        ]);
        ingredients.setId(result.insertId)
        return ingredients.getId();
    }
    async update(ingredients: Ingredients) {
        const result = await this.query('UPDATE ingredients SET name=? WHERE id=?', [ingredients.getName(),ingredients.getId()])

        return result
    }
    async delete(ingredients:Ingredients){
        await this.query('DELETE FROM ingredients WHERE id=?',[ingredients.getId])
    }
}
