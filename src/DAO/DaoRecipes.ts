import { connection } from "../ConnectDB/ConnectDB"
import { promisify } from "util";
import { Recipes } from "../Entity/Recipes";




export class DaoRecipes {
    private query;
    constructor() {
        this.query = promisify(connection.query).bind(connection);
    }
    async getAll(): Promise<Recipes[]> {
        let result = await this.query("SELECT * FROM recipes");

        return result.map(row => new Recipes(row['name'], row['category'], row['picture'], row['score'], row['id']));

    }
    async getByID(id:number):Promise<Recipes> {
        let results = await this.query("SELECT * FROM recipes WHERE id=?",[id]);
        if(results.length > 0) {
            return new Recipes(results[0]['name'], results[0]['category'], results[0]['picture'],results[0]['score'],results[0]['id']);
        }
        return null;
    }
    async getByName(name:string):Promise<Recipes> {
        let results = await this.query("SELECT * FROM recipes WHERE name LIKE ?",[name]);
        if(results.length > 0) {
            return new Recipes(results[0]['name'], results[0]['category'], results[0]['picture'],results[0]['score'],results[0]['id']);
        }
        return null;
    }
    async add(recipes: Recipes): Promise<number> {
        let result = await this.query('INSERT INTO recipes (name,category,picture,score) VALUES (?,?,?,?)', [
            recipes.getName(),
            recipes.getCategory(),
            recipes.getPicture(),
            recipes.getScore()
        ]);
        recipes.setId(result.insertId)
        return recipes.getId();
    }
    async update(recipe: Recipes) {
        const result = await this.query('UPDATE recipes SET name=?,category=?,picture=?,score=? WHERE id=?', [recipe.getName(), recipe.getCategory(), recipe.getPicture(), recipe.getScore(),recipe.getId()])

        return result
    }
    async delete(recipes:Recipes){
        await this.query('DELETE FROM recipes WHERE id=?',[recipes.getId])
    }
}
