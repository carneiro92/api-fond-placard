import express = require("express");
import { ingredientsRouter } from "./Controller/ingredientsController";
import { recipesRouter } from "./Controller/recipesController";
const bodyParser = require('body-parser');

export class App {
    
    private app: express.Application;

    constructor(){
        this.app = express();
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));


    }
    async listen(){
        await this.app.listen(3000);
        console.log('Server on port',3000)
        
    }
    registerRouter(){
        this.app.use(recipesRouter)
        this.app.use(ingredientsRouter)
    }
    

}