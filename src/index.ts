import { App } from './app'


async function main() {
    const app = new App();
    app.registerRouter()
    await app.listen();
}

main();