import { Router } from "express";
import { IngredientsBusiness } from "../Business/ingredientsBusiness";
import { Ingredients } from "../Entity/Ingredients";

export const ingredientsRouter = Router();
const ingredientsBusiness = new IngredientsBusiness();

ingredientsRouter.get("/ingredients", async (req, res) => {
  if (req.query.name) {
    const ingredient = await ingredientsBusiness.getByName(
      <string>req.query.name
    );
    if (!ingredient) {
      res.status(404).end();
      return;
    }
    res.json(ingredient);
  }
  try {
    let ingredients = await ingredientsBusiness.getAll();
    res.json(ingredients);
  } catch (error) {
    res.status(500).json(error);
  }
});
ingredientsRouter.get("/ingredients/:id", async (req, res) => {
  try {
    const ingredient = await ingredientsBusiness.getByID(
      Number.parseInt(req.params.id)
    );
    res.json(ingredient);
  } catch (error) {res.status(404).json(error);}
});
ingredientsRouter.post("/ingredients", async (req, res) => {
  const ingredient = new Ingredients(req.body.name);
  ingredientsBusiness
    .add(ingredient)
    .then((data) => res.status(201).json(data));
});
ingredientsRouter.patch("/ingredients/:id", async (req, res) => {
  let ingredient = ingredientsBusiness.update(req);
  res.json(ingredient);
});
ingredientsRouter.delete("/ingredients/:id", async (req, res) => {
  const ingredient = await ingredientsBusiness.getByID(
    Number.parseInt(req.params.id)
  );
  if (!ingredient) {
    res.status(404).end();
    return;
  }
  ingredientsBusiness.delete(ingredient);
  res.status(204);
  res.statusMessage = "Deleted!";
});
