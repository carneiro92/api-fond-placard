import { Router } from "express";
import { RecipesBusiness } from "../Business/recipesBusiness";

import { Recipes } from "../Entity/Recipes";

export const recipesRouter = Router();
const recipesBusiness = new RecipesBusiness

recipesRouter.get('/recipes', async (req, res) => {
    if (req.query.name) {
        const recipe = await recipesBusiness.getByName(<string>req.query.name);
        if (!recipe) {

            res.status(404).end();
            return;
        }
        res.json(recipe)
    }
    try {
        let recipes = await recipesBusiness.getAll();
        res.json(recipes);
    } catch (error) {
        res.status(500).json(error)
    }

});
recipesRouter.get("/recipes/:id", async (req, res) => {
    const recipe = await recipesBusiness.getByID(Number.parseInt(req.params.id))
    res.json(recipe);
});
recipesRouter.post('/recipes', async (req, res) => {
    const recipe = new Recipes(req.body.name, req.body.category, req.body.picture, req.body.score)
    recipesBusiness.add(recipe).then(data => res.status(201).json(data));

});
recipesRouter.patch('/recipes/:id', async (req, res) => {
    const recipe = await recipesBusiness.getByID(Number.parseInt(req.params.id));
    if (!recipe) {

        res.status(404).end();
        return;
    }
    recipe.update(req.body)

    recipesBusiness.update(recipe);
    res.json(recipe);
});
recipesRouter.post('/recipes/:id/upvote', async (req, res) => {
    const recipe = await recipesBusiness.getByID(Number.parseInt(req.params.id));
    const upvoted = await recipesBusiness.upvote(recipe)
    res.json(upvoted)
});
recipesRouter.post('/recipes/:id/downvote', async (req, res) => {
    const recipe = await recipesBusiness.getByID(Number.parseInt(req.params.id));
    const downvoted = await recipesBusiness.downvote(recipe)
    res.json(downvoted)
});
recipesRouter.delete('/recipes/:id', async (req, res) => {
    const ingredient = await recipesBusiness.getByID(Number.parseInt(req.params.id));
    if (!ingredient) {

        res.status(404).end();
        return;
    }
    recipesBusiness.delete(ingredient)
    res.status(204)
    res.statusMessage = "Deleted!"

})
