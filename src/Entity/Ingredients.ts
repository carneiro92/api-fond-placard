export class Ingredients {
    constructor(private name:string,
                private id?:number){}

    getName(){
        return this.name;
    }
    setName(name:string) {
        this.name = name;
    }
    getId() {
        return this.id;
    }
    setId(id:number) {
        this.id=id;
    }
    update(data:Ingredients){
        Object.assign(this,data)
    }
}
