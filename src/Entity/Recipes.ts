export class Recipes {
    constructor(private name: string,
        private category: string,
        private picture: string,
        private score: number,
        private id?: number) {
            this.id = id,
            this.name = name,
            this.category = category,
            this.picture = picture,
            this.score = score
    }

    getName() {
        return this.name;
    }
    setName(name: string) {
        this.name = name
    }
    getCategory() {
        return this.category;
    }
    setCategory(category: string) {
        this.category = category
    }
    getPicture() {
        return this.picture;
    }
    setPicture(picture: string) {
        this.picture = picture
    }
    getScore() {
        return this.score;
    }
    setScore(score: number) {
        this.score = score
    }
    getId() {
        return this.id;
    }
    setId(id: number) {
        this.id = id
    }
    update(data:Recipes){
        Object.assign(this,data)
    }
}
